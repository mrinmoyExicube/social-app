import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LogoutPage } from '../pages/logout/logout';
import { PostsPage } from '../pages/posts/posts';
import { ProfilePage } from '../pages/profile/profile';
import { LoginPage } from '../pages/login/login';
import { RegistrationPage } from '../pages/registration/registration';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { CreatePostPage } from '../pages/create-post/create-post';
import { SinglePostPage } from '../pages/single-post/single-post';

import * as firebase from 'firebase';
import { ProfileEditPage } from '../pages/profile-edit/profile-edit';
import { CommentPageModule } from '../pages/comment/comment.module';
import { ShowCommentsPage } from '../pages/show-comments/show-comments';


  var config = {
    apiKey: "AIzaSyCRGZBMx11t_QNgmWuptavOq9NnDhv-uyA",
    authDomain: "socialapp-918de.firebaseapp.com",
    databaseURL: "https://socialapp-918de.firebaseio.com",
    projectId: "socialapp-918de",
    storageBucket: "socialapp-918de.appspot.com",
    messagingSenderId: "1077349729681"
  };
firebase.initializeApp(config);


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LogoutPage,
    PostsPage,
    ProfilePage,
    LoginPage,
    RegistrationPage,
    ForgotPasswordPage,
    CreatePostPage,
    SinglePostPage,
    ProfileEditPage,
    ShowCommentsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    CommentPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LogoutPage,
    PostsPage,
    ProfilePage,
    LoginPage,
    RegistrationPage,
    ForgotPasswordPage,
    CreatePostPage,
    SinglePostPage,
    ProfileEditPage,
    ShowCommentsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    
  ]
})
export class AppModule {}
