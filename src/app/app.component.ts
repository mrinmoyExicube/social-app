import { Component, ViewChild, NgZone } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
//import { ListPage } from '../pages/list/list';
import { LogoutPage } from '../pages/logout/logout'

import { PostsPage } from '../pages/posts/posts';
import { ProfilePage } from '../pages/profile/profile';
import { LoginPage } from '../pages/login/login';
import { ShowCommentsPage } from '../pages/show-comments/show-comments'

//firebase
import * as firebase from 'firebase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // rootPage: any = LoginPage;
  rootPage: any ;


  pages: Array<{title: string, component: any}>;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public zone:NgZone) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Profile', component: ProfilePage },
      { title: 'My posts', component: PostsPage },
      //{ title: 'List', component: ListPage }
      { title: 'Logout', component: LogoutPage },
     
    
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.checkLoginStatus();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  checkLoginStatus(){
    firebase.auth().onAuthStateChanged((user)=>{
      if(user){
        this.zone.run(()=>{ this.rootPage = HomePage })
      }else{ this.zone.run(()=>{ this.rootPage = LoginPage;}) }
    })

  }

  openPage(page) {
    if(page.title == "Logout"){
      firebase.auth().signOut();
    }else{
      this.nav.setRoot(page.component);
    }
  }

  
}
