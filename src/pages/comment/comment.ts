import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase';

/**
 * Generated class for the CommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-comment',
  templateUrl: 'comment.html',
})
export class CommentPage {
    comment:string;
    postkey:any;
    comments:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    var data = this.navParams.get('individualPost');
     console.log(data)
     console.log(data.postkey)
     this.postkey = data.postkey

     let new_ref = firebase.database().ref('all-post/'+this.postkey + '/comments');
     new_ref.on('value',(snapshot:any)=>{
     let newcomments = snapshot.val();
     console.log(newcomments)
     this.comments = [];
       for(let key in newcomments){
       this.comments.push (newcomments[key])
       }
     console.log( 'all comments',this.comments)

     })


     
  }

  CloseModal(){
    this.navCtrl.pop();
  }
   
  CommentSumit(){
    firebase.database().ref('all-post/'+this.postkey + '/comments')
     .push({comment:this.comment})
     this.comment = '' 
  }


      


 
}
