import { CommentPage } from './../comment/comment';
import { Component } from '@angular/core';
import { NavController, ModalController  } from 'ionic-angular';
import { CreatePostPage } from '../create-post/create-post';

import * as firebase from 'firebase';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  posts:any;
  postId:any;
  userId:any;
  comment:string;
  
  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {
    this.userId = firebase.auth().currentUser.uid
    let ref = firebase.database().ref('all-post/');
    ref.on('value',(snapshot:any)=>{
     if(snapshot.val()){
       let newposts = snapshot.val();
       this.posts = [];

       for( let key in newposts){
         newposts[key].postkey = key

          /* For like count */
         if(newposts[key].alllikes){
           let data = newposts[key].alllikes;
           let count:number= 0;
           for( let key in data){
            count = count+1;
           }
           newposts[key].likecount = count;
         }else{
          newposts[key].likecount = 0;
         }
          /* For like count */

        this.posts.push (newposts[key])
        this.postId = key
      }
      console.log(this.posts);
      }

    })

  }


  postModal() {
    const modal = this.modalCtrl.create(CreatePostPage);
    modal.present();
  }

 

  Counter(individualPost){
    let ref = firebase.database().ref('all-post/'+individualPost.postkey + '/alllikes/'+this.userId + '/');
    ref.once('value',(snap1:any)=>{
      if(snap1.val()){
        firebase.database().ref('all-post/'+individualPost.postkey + '/alllikes/'+this.userId + '/')
        .remove();
      }else{
          firebase.database().ref('all-post/'+individualPost.postkey + '/alllikes/'+this.userId + '/')
          .set({like:true})
      }
    })
  }

  CommentModal(individualPost) {
    const modal = this.modalCtrl.create(CommentPage,{individualPost:individualPost});
    modal.present();
  }

 

    
  



}


