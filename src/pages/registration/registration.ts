import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import * as firebase from 'firebase';

@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {

  public name:string;
  public email:string;
  public password:string;
  public address:string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams
  ) {
  }

  SignUp(){
    //Create User using Firebase
    firebase.auth().createUserWithEmailAndPassword(this.email, this.password)
    .then((usercredential:any)=>{
      console.log('usercredential',usercredential.user.uid);
      if(usercredential){
        firebase.database().ref('/users/'+usercredential.user.uid+ '/')
        .set({
          useremail:this.email,
          userfullname:this.name,
          useraddress:this.address
        })
      }
    })
    .catch((error)=>{console.log(error)})

  }
  

}
