import { Component } from '@angular/core';
import { NavController, NavParams,ModalController } from 'ionic-angular';
import { RegistrationPage } from '../registration/registration'
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { HomePage } from '../home/home';
import * as firebase from 'firebase';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public email:string;
  public password:string;


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public modalCtrl: ModalController
  ) { }

  presentModal() {
    const modal = this.modalCtrl.create(ForgotPasswordPage);
    modal.present();
  }

  goToRegistrationPage(){
    this.navCtrl.push(RegistrationPage)
  }

  signIn(){
    //Signin User using Firebase
      firebase.auth().signInWithEmailAndPassword(this.email, this.password)
      .then((usercredential:any)=>{ console.log(usercredential) })
      .catch((error)=>{console.log(error)})
  }

}
