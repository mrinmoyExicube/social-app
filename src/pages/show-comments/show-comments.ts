import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase';

/**
 * Generated class for the ShowCommentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-show-comments',
  templateUrl: 'show-comments.html',
})
export class ShowCommentsPage {
  comments:any;
  posts:any;
  postId:any;
  postkey:any;
 
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    let ref = firebase.database().ref('all-post/');
    ref.on('value',(snapshot:any)=>{
     if(snapshot.val()){
       let newposts = snapshot.val();
       this.posts = [];
       console.log(newposts)
       for( let key in newposts){
        newposts[key].postkey = key
        this.posts.push (newposts[key])
        this.postId = key
      }
      console.log(this.posts);
      
      }

        let new_ref = firebase.database().ref('all-post/'+this.postId + '/comments');
        new_ref.on('value',(snapshot:any)=>{
        let newcomments = snapshot.val();
        console.log(newcomments)
        this.comments = [];
          for(let key in newcomments){
          this.comments.push (newcomments[key])
          }
        console.log( 'all comments',this.comments)

        })


    })
   


  }

  

}
