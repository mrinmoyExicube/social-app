import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import * as firebase from 'firebase';
import { ProfileEditPage } from "../profile-edit/profile-edit";
import { Camera, CameraOptions } from '@ionic-native/camera';


/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  public userId:any;
  singleuser={};
  //image: string;
  clickedImagePath:any;
  

  options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    cameraDirection:0
  }

    
  
  
  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController, public navParams: NavParams, public modalCtrl: ModalController, private camera: Camera) {
    let user = firebase.auth().currentUser;
    if(user){
      console.log("user:", user)
      let ref = firebase.database().ref('users/'+ user.uid +'/');
      ref.once('value',(snapshot:any)=>{
        if(snapshot.val()){
          let singleuser = snapshot.val();
          this.singleuser = singleuser; 
          console.log(snapshot.val())
        }
      })
    }else{console.log("user: not found..")}

  }

  clickImage(){
    this.camera.getPicture(this.options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.clickedImagePath = 'data:image/jpeg;base64,' + imageData;
      
     }, (err) => {
      // Handle error
     });
  }
  
  ProfileModal() {
    const modal = this.modalCtrl.create(ProfileEditPage);
    modal.present();
  }
  
  


  


  

}
