import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase';
/**
 * Generated class for the PostsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 
@Component({
  selector: 'page-posts',
  templateUrl: 'posts.html',
})
export class PostsPage {
    posts:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    let userid = firebase.auth().currentUser.uid  
    console.log(userid)

    let ref = firebase.database().ref('all-post/');
    ref.once('value',(snapshot:any)=>{
     if(snapshot.val()){
       console.log(snapshot.val())
       let newposts = snapshot.val();
       this.posts = [];
       for( let key in newposts){
         if(newposts[key].userid == userid){
          this.posts.push(newposts[key])
         }
       }
       console.log(this.posts)

      }

    })
    

  }
    
 

   

    

}
