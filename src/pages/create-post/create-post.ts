import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase';

/**
 * Generated class for the CreatePostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-create-post',
  templateUrl: 'create-post.html',
})
export class CreatePostPage {
    posts:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  
  }

  title:string;
  description:string;
  comment:string;

  
  closePostModal(){
    this.navCtrl.pop();
  }

    PostShare(){
     let userid = firebase.auth().currentUser.uid
     console.log(userid);
      firebase.database().ref('all-post/')
      .push({
        title:this.title,
        description:this.description,
        userid ,
      })

    .catch((error)=>{console.log(error)})

    }

    
     
  
   


  }



